
/* eslint quotes: 0 */
// Defines the MongoDB $jsonSchema for service `users`. (Can be re-generated.)
const merge = require('lodash.merge');
// !code: imports // !end
// !code: init // !end

let moduleExports = merge({},
  // !<DEFAULT> code: model
  {
    bsonType: "object",
    additionalProperties: false,
    properties: {
      _id: {
        bsonType: "objectId"
      },
      email: {
        minLength: 4,
        maxLength: 50,
        bsonType: "string"
      },
      name: {
        minLength: 50,
        maxLength: 80,
        bsonType: "string"
      },
      password: {
        minLength: 20,
        maxLength: 255,
        bsonType: "string"
      }
    },
    required: [
      "name",
      "email",
      "password"
    ]
  },
  // !end
  // !code: moduleExports // !end
);

// !code: exports // !end
module.exports = moduleExports;

// !code: funcs // !end
// !code: end // !end
