
/* eslint quotes: 0 */
// Defines the MongoDB $jsonSchema for service `post`. (Can be re-generated.)
const merge = require('lodash.merge');
// !code: imports // !end
// !code: init // !end

let moduleExports = merge({},
  // !<DEFAULT> code: model
  {
    bsonType: "object",
    additionalProperties: false,
    properties: {
      _id: {
        bsonType: "objectId"
      },
      title: {
        minLength: 4,
        maxLength: 50,
        bsonType: "string"
      },
      body: {
        bsonType: "string"
      },
      author: {
        bsonType: "objectId"
      },
      category: {
        bsonType: "objectId"
      }
    },
    required: [
      "category",
      "author",
      "title",
      "body"
    ]
  },
  // !end
  // !code: moduleExports // !end
);

// !code: exports // !end
module.exports = moduleExports;

// !code: funcs // !end
// !code: end // !end
