
// Hooks for service `post`. (Can be re-generated.)
const commonHooks = require('feathers-hooks-common');
// !code: imports
const { authenticate } = require('@feathersjs/authentication').hooks;
const verifyEmail = require('../users/hooks/verify-email');
// !end

// !<DEFAULT> code: used
// eslint-disable-next-line no-unused-vars
const { iff } = commonHooks;
// eslint-disable-next-line no-unused-vars
const { create, update, patch, validateCreate, validateUpdate, validatePatch } = require('./post.validate');
// !end

// !code: init
const postResolvers = {
  joins: {
    author: (...args) => async (post,context) => {    
      post.author = (await context.app.service('users').find({
      query: {id: post.author},
      paginate: false
    }))[0] },
    
    category: (...args) => async (post,context) => { post.category = (await context.app.service('category').find({
      query: {id: post.category},
      paginate: false
    }))[0] },
  }
};
// !end

let moduleExports = {
  before: {
    // !code: before
    all: [],
    find: [],
    get: [verifyEmail],
    create: [validateCreate(),authenticate('jwt')],
    update: [validateUpdate(),authenticate('jwt')],
    patch: [validatePatch(),authenticate('jwt')],
    remove: []
    // !end
  },

  after: {
    // !<DEFAULT> code: after
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
    // !end
  },

  error: {
    // !<DEFAULT> code: error
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
    // !end
  },
  // !code: moduleExports // !end
};

// !code: exports // !end
module.exports = moduleExports;

// !code: funcs // !end
// !code: end // !end
